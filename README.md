# Tea bag dryer

This is a dryer for tea bags. There are several reasons why you want to dry
used tea bags. For example, so that they do not start to get moldy in the
trash can. There are also different applications for dried tea bags, just
google for it.

![](./img1.jpg)

I created the tea bag dryer in two versions: with and without a bottom. The
one without a bottom is best placed on the drying area at the sink.

Published on [Thingiverse][1] on 10.10.2020.

License: [CC BY-SA 4.0][2]

[1]: https://www.thingiverse.com/thing:4620241
[2]: https://creativecommons.org/licenses/by-sa/4.0/

